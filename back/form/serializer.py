from rest_framework import serializers
from .models import Resume, Appraisal

class ResumeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Resume
        fields = ('id', 'name', 'email', 'mobile', 'is_review', 'appraisal')

class AppraisalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appraisal
        fields = ('resume', 'first', 'second', 'third')