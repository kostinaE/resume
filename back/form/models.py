from django.db import models
import os
from django.conf import settings

def directory_path(instance, filename):
    print(instance)
    return os.path.join(settings.RESUME_IMG_URL, filename)

class Resume(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    mobile = models.CharField(max_length=200, default='')
    image_first = models.ImageField(blank=True, null=True, upload_to=directory_path)
    image_second = models.ImageField(blank=True, null=True, upload_to=directory_path)
    is_review = models.BooleanField(default=False) 

    def __str__(self):
       return self.email

class Appraisal(models.Model):
    resume = models.OneToOneField(Resume, on_delete=models.CASCADE, primary_key = True)
    first = models.PositiveSmallIntegerField(default=0)
    second = models.PositiveSmallIntegerField(default=0)
    third = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
       return str(self.first)
