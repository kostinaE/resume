from django_cron import CronJobBase, Schedule
from django.core.mail import send_mail
from form.models import Resume


class MailSender(CronJobBase):
    RUN_EVERY_MINS = 1

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'resume.mailsender'    # a unique code

    def do(self):
        resumes_onreview = Resume.objects.filter(is_review=False)
        message = 'Количество не проверенных работ: ' + str(len(resumes_onreview))

        send_mail(
        'Отчет о непроверенных работах',
        message,
        'cat.812@yandex.ru',
        ['evkostina2@gmail.com'],
        fail_silently=False,
    )   