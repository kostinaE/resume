import Vue from 'vue';
import { config } from './apiConfig';

export default {
    updateAppraisal(data) {
        return Vue.axios.post('api/appraisal/update/', data, config);
    },
}