from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login, logout, password_validation
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
import json

@csrf_exempt
def sing_in(request):
    json_data = json.loads(request.body)
    email = json_data['email']
    password = json_data['password']

    try:
        u = User.objects.get(email=email)
    except User.DoesNotExist:
        pass
    else:
        email = u.email

    user = authenticate(
        username=email,
        password=password
    )

    if user is not None:
        token, created = Token.objects.get_or_create(user=user)
        if created: token.save()
        return JsonResponse({"token": token.key})
    else:
        return JsonResponse({"error": "incorrect data"}, status=401)
