from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from PIL import Image
from django.conf import settings
from reportlab.pdfgen import canvas

from form.models import Resume, Appraisal
from .serializer import ResumeSerializer, AppraisalSerializer
import json


@csrf_exempt
def get_all_resume(request):
    resumes = Resume.objects.all()
    data = []
    for resume in resumes:
        data.append({
            'id': resume.id,
            'name': resume.name,
            'email': resume.email,
            'is_review': resume.is_review,
            'appraisal': {
                'first': resume.appraisal.first,
                'second': resume.appraisal.second,
                'third': resume.appraisal.third
            }
        })
    return JsonResponse({"resumes": data})

@csrf_exempt
def onreview(request):
    resumes_onreview = Resume.objects.filter(is_review=False)
    return JsonResponse({"onreview": ResumeSerializer(resumes_onreview, many=True).data})

@csrf_exempt
def get_resume_by_id(request):
    json_data = json.loads(request.body)
    resume_id = json_data['id']
    resume = Resume.objects.get(id=resume_id)
    save_resume_pdf(resume.image_first, resume.name, resume.email)

    data = {
            'name': resume.name,
            'email': resume.email,
            'is_review': resume.is_review,
            'image_first': request.build_absolute_uri(resume.image_first.url) if resume.image_first else '',
            'image_second': request.build_absolute_uri(resume.image_second.url) if resume.image_second else '',
            'appraisal': {
                'first': resume.appraisal.first,
                'second': resume.appraisal.second,
                'third': resume.appraisal.third
            }
    }
    return JsonResponse(data)

def save_resume_pdf(resume_image, resume_name, resume_email):
    image = Image.open(resume_image)
    width, height = image.size  
    left = width/4
    top = height/4
    right = left + 200
    bottom = top + 200
    cropped_image = image.crop((left, top, right, bottom))
    path = settings.RESUME_PDF
    path = ''.join([path, resume_name, '_', resume_email, '.pdf']) 
    cropped_image.save(path, save_all=True)


@csrf_exempt
def update_appraisal(request):
    json_data = json.loads(request.body)
    resume_id = json_data['resume_id']
    appraisal = Appraisal.objects.get(resume=resume_id)

    if 'first' in json_data:
        appraisal_first = json_data['first']
        appraisal.first = appraisal_first

    if 'second' in json_data:
        appraisal_second = json_data['second']
        appraisal.second = appraisal_second

    if 'third' in json_data:
        appraisal_third = json_data['third']
        appraisal.third = appraisal_third 
    if appraisal.first != 0 and appraisal.second != 0 and appraisal.third != 0:
        appraisal.resume.is_review = True
        appraisal.resume.save()
    appraisal.save()
    return HttpResponse(status=200)


@csrf_exempt
def add_new_resume(request):
    name = request.POST.get('name', '')
    mobile = request.POST.get('mobile', '')
    email = request.POST.get('email', '')
    image_first = request.FILES.get('img_first', [])
    image_second = request.POST.get('img_second', [])

    new_resume = Resume(
        name=name,
        mobile=mobile,
        email=email,
        image_first=image_first,
        image_second=image_second,
        is_review=False
    )
    new_resume.save()
    appraisal = Appraisal(
        resume=new_resume,
        first=0,
        second=0,
        third=0
    )
    appraisal.save()
    return JsonResponse({name: name}, status=200)
