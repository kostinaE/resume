from django.contrib import admin

from .models import Resume, Appraisal

admin.site.register(Resume)
admin.site.register(Appraisal)
