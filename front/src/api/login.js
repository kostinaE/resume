import Vue from 'vue';
import { config } from './apiConfig';

export default {
    signIn(data) {
        return Vue.axios.post('login/signin/', data, config);
    }
}