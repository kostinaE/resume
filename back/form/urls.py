from django.urls import path

from . import views

urlpatterns = [
    path('appraisal/update/', views.update_appraisal),
    path('resume/create/', views.add_new_resume),
    path('onreview', views.onreview),
    path('resume_by_id', views.get_resume_by_id),
    path('resumes/', views.get_all_resume),
]