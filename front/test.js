const { Builder, By, Key, until } = require('selenium-webdriver');

(async function example() {
  let driver = await new Builder().forBrowser('chrome').build();
  await driver.get('http://localhost:8080/login');
  await driver.findElement(By.id('login_email')).sendKeys('test@test.ru');
  await driver.findElement(By.id('login_password')).sendKeys('_test123456');
  await driver.findElement(By.css('.btn-lg')).click();

  await driver.manage().setTimeouts({ implicit: 100 });
  driver.findElement(By.css('.alert')).getText()
    .then(function (txt) {
      console.log(txt);
    },
      err => {
        console.log('Пароль верный, перешли на /admin');
      });
})();