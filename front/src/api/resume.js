import Vue from 'vue';
import { config } from './apiConfig';

export default {
    saveNewResume(data) {
        return Vue.axios.post('api/resume/create/', data, config);
    },
    getAllResumes() {
        return Vue.axios.get('api/resumes/', config);
    },
    getResumeBuId(id) {
        return Vue.axios.post('api/resume_by_id', { id: id }, config);
    }
}