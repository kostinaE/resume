from django.test import TestCase
from form.models import Resume, Appraisal


class ModelTest(TestCase):
      def setUp(self):
         new_resume = Resume(
            name='Ekaterina',
            mobile='89996665522',
            email='ektr@yandex.ru',
            is_review=False
         )
         new_resume.save()
         appraisal = Appraisal(
             resume=new_resume,
             first=0,
             second=0,
             third=0
         )
         appraisal.save()


      def test_onreview_resume(self):
        resumes_onreview_before = len(Resume.objects.filter(is_review=False))
        appraisal = Appraisal.objects.all()[0]
        appraisal.first = 2
        appraisal.second = 3
        appraisal.third = 4
        appraisal.resume.is_review = True
        appraisal.save()
        appraisal.resume.save()
        resumes_onreview_after = len(Resume.objects.filter(is_review=False))
        print(resumes_onreview_before - resumes_onreview_after == 1)
