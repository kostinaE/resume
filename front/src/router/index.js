import Vue from 'vue';
import Router from 'vue-router';
import FillResume from '../components/FillResume';
import ListResumes from '../components/ListResumes';
import LoginAdmin from '../components/LoginAdmin';
import ResumeReview from '../components/ResumeReview';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'resume', component: FillResume },
    { path: '/login', name: 'login', component: LoginAdmin },
    { path: '/admin', name: 'admin', component: ListResumes },
    { path: '/admin/review/:id', name: 'review', component: ResumeReview },
    { path: '*', redirect: { name: 'resume' } },
  ],
});