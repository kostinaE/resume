import Vue from 'vue';

const needSignUp = () => {
    var jwt = localStorage.getItem('auth_token')
    return !jwt;
}

export default needSignUp;